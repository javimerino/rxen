//! Xen-specific unikernel based on `libxr`
//!
//! This is a proof of concept to show the capabilities of a Rust-based
//! ABI substrate.
#![no_std]
#![no_main]

unsafe extern "C" fn _start() -> ! {
    loop {
        core::arch::asm!("hlt");
    }
}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    loop {
        unsafe {
            core::arch::asm!("hlt");
        }
    }
}
