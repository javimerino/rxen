# Add --release to build in release mode instead
buildtype := ""

clippy_args := "-D clippy::all -D missing_docs -D warnings -A dead-code"
target := "x86_64-unknown-none"

clippy:
    cd xr && cargo clippy -- {{clippy_args}}
    cd xrtf && cargo clippy --target {{target}} -- {{clippy_args}}
    cd libxr && cargo clippy -- {{clippy_args}}
    cd libxr && cargo clippy -- {{clippy_args}} --target {{target}}

checkfmt:
    cd libxr && cargo fmt --check
    cd xr && cargo fmt --check
    cd xrtf && cargo fmt --check

fmt:
    cd libxr && cargo fmt
    cd xr && cargo fmt
    cd xrtf && cargo fmt

build:
    cd xr && cargo rustc {{buildtype}} -- -C target-feature=+crt-static
    cd xrtf && cargo build {{buildtype}} --target {{target}}

ci:
    just checkfmt
    just build
    just clippy

clean:
    rm -rf libxr/target/
    rm -rf xr/target/
    rm -rf xrtf/target/
