//! Rust bindings for communicating with the Xen hypervisor
//!
//! This crate is meant to be used in any of the following configurations:
//!   * hosted: `std`, and hypercalls mediated through `/dev/xen/privcmd`.
//!   * freestanding: `no_std` and direct hypercalls. Meant for unikernels.

#![cfg_attr(target_os = "none", no_std)]

pub mod hyp;

pub mod domctl;
pub mod sysctl;

#[cfg(not(target_os = "none"))]
pub mod std;

/// Wrapper around a Xen domain identifier for type-safety.
#[repr(transparent)]
#[derive(Debug, Copy, Clone)]
pub struct DomId(pub u16);

// Public re-exports
pub use hyp::XenCall;
