use nix::errno::Errno;

use crate::{hyp::XenInterface, sysctl};

pub enum Error {
    HyperCallError(Errno),
    MalformedReply,
}

pub fn try_dmesg(xi: &XenInterface) -> Option<String> {
    // Don't use zeroes in order to force the prefaulting of pages
    let mut buffer = vec![u8::MAX; 16 << 10];
    let mut dmesg = String::with_capacity(buffer.len());

    let mut sysctl = sysctl::ReadConsoleRaw {
        hdr: sysctl::SysCtlHeader::new(),
        incremental: true,
        clear: false,
        _pad0: 0,
        index: 0,
        buffer: buffer.as_mut_ptr() as u64,
        count: buffer.capacity().try_into().unwrap_or(u32::MAX),
        _pad1: unsafe { core::mem::zeroed() },
    };

    while sysctl.count > 0 {
        if let Err(rc) = unsafe { sysctl.read(xi) } {
	    let errno = nix::errno::from_i32(rc);
	    eprintln!("[ERROR] readconsole rc=({errno})");
	    return None;
	}
        buffer.truncate(sysctl.count as usize);
        let Ok(s) = std::str::from_utf8(&buffer) else {
            println!("[ WARN] readconsole invalid_utf8");
            return None;
        };
	dmesg += s;
    }
    Some(dmesg)
}
