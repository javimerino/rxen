//! Binding for the `xen_sysctl_readconsole` buffer type
//!
//! Enables the `XEN_SYSCTL_readconsole` hypercall

use crate::hyp::{IsSysCtl, XenCall, XenInterface};

use super::SysCtlHeader;

/// Buffer type for a read request into the hypervisor log buffer. This is
/// typically queried from userspace via something like `xr dmesg`
#[repr(C)]
#[derive(Debug)]
pub struct ReadConsoleRaw {
    /// Common header to every `sysctl`. See [`SysCtlHeader`].
    pub hdr: SysCtlHeader,
    /// IN: Clear the console after reading if set.
    pub clear: bool,
    /// IN: Start reading at offset [`index`] if set.
    pub incremental: bool,
    /// Unused.
    pub _pad0: u16,
    /// IN: Start index for consuming from ring buffer (if [`incremental`] is set).
    /// OUT: End index after consuming from ring buffer.
    pub index: u32,
    /// IN: Virtual address to write console data. */
    pub buffer: u64,
    /// IN: Size of buffer.
    /// OUT: Bytes written to buffer.
    pub count: u32,
    /// Unused.
    pub _pad1: [u8; 108],
}

impl ReadConsoleRaw {
    /// Identifies which particular `sysctl` this hypercall is
    const SYSCTL_OP: u32 = 1;

    /// Reads the hypervisor ring console
    ///
    /// # Safety
    /// Same as [`XenCall::sysctl`]
    pub unsafe fn read(&mut self, xi: &XenInterface) -> Result<(), i32> {
        self.hdr.op = Self::SYSCTL_OP;
        xi.sysctl(self)
    }
}

use core::mem::size_of;
const _: () = assert!(size_of::<ReadConsoleRaw>() - size_of::<SysCtlHeader>() == 128);

impl IsSysCtl for ReadConsoleRaw {}
