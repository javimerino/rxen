//! Abstraction over `domctl` Xen hypercalls

pub use crate::DomId;

pub mod cpu_policy;
pub use cpu_policy::CpuPolicyRaw;

/// Header common to every `domctl` (See [`XXX`], for example). This is
/// the common header that identifier the operation and the layout of the passed
/// data.
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct DomCtlHeader {
    /// Identifies which particular `domctl` must be performed and
    /// which variant follows this header. The hypervisor is meant to read this
    /// value and find the meaning of the bits that follow it (See
    /// [`XXX`] for an example. Think single-level inheritance.
    op: u32,
    /// Must match [`DomCtlHeader::INTERFACE_VERSION`]. Used to tell the
    /// hypervisor the expected ABI.
    interface_version: u32,
    /// Must match [`DomCtlHeader::INTERFACE_VERSION`]. Used to tell the
    /// hypervisor the expected ABI.
    domid: DomId,
    /// Unused.
    _pad0: [u8; 6],
}

impl DomCtlHeader {
    /// Xen doesn't have a stable ABI and bumps this number whenever the ABI
    /// changes. When a hypercall lands on the hypervisor, it compares its own
    /// internal number with this one, and will fail early with `-EACCES` if
    /// they don't match.
    const INTERFACE_VERSION: u32 = 0x15;

    /// Creates a valid `domctl` header for a specific `cmd`
    pub const fn new() -> Self {
        let interface_version = Self::INTERFACE_VERSION;
        Self {
            op: u32::MAX,           // poison
            domid: DomId(u16::MAX), // poison,
            interface_version,
            _pad0: [0; 6],
        }
    }
}
