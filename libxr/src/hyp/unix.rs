//! UNIX Xen interface
//!
//! Implementation of [`XenInterface`] for UNIX-like systems. Ought to work on both Linux
//! and *BSD systems implementing `/dev/xen/privcmd`.
//!
//! # TODO
//!   1. Use `/dev/xen/hypercall` on Linux-based systems.

use std::{fs::File, os::fd::AsRawFd};

use super::{IsDomCtl, IsSysCtl, XenCall};

/// An abstraction over
#[derive(Debug)]
pub struct XenInterface(File);

#[cfg(target_os = "linux")]
type PrivCmdField = u64;

#[cfg(not(target_os = "linux"))]
type PrivCmdField = usize;

#[repr(C)]
#[derive(Debug)]
struct PrivCmdArg {
    /// Identifier for the issued hypercall type
    op: PrivCmdField,
    /// Hypercall-specific arguments
    args: [PrivCmdField; 5],
    #[cfg(not(target_os = "linux"))]
    /// Return code of the `ioctl` in *BSD systems
    ret: PrivCmdField,
}

mod ioctl {
    #![doc(hidden)]
    use super::*;

    nix::ioctl_write_ptr_bad!(
        hypercall,
        nix::ioc!(0, b'P', 0, core::mem::size_of::<PrivCmdArg>()),
        PrivCmdArg
    );
}

/// Path to the `privcmd` file in a hosted environment.
const PATH_PRIVCMD: &str = "/dev/xen/privcmd";

impl XenCall for XenInterface {
    fn try_new() -> Option<Self> {
        let path = PATH_PRIVCMD;
        match File::options().append(true).open(path) {
            Ok(proto) => Some(Self(proto)),
            Err(_) => {
                println!("[ERROR]  Can't open {path}");
                None
            }
        }
    }

    unsafe fn sysctl<T: IsSysCtl>(&self, t: &mut T) -> Result<(), i32> {
        use core::ptr::addr_of_mut;
        let mut privcmd_arg = PrivCmdArg {
            op: super::SYSCTL_CMD as PrivCmdField,
            args: [addr_of_mut!(*t) as PrivCmdField, 0, 0, 0, 0],
            #[cfg(not(target_os = "linux"))]
            ret: 0,
        };

        match ioctl::hypercall(self.0.as_raw_fd(), addr_of_mut!(privcmd_arg)) {
            Ok(_) => Ok(()),
            Err(errno) => Err(errno as i32),
        }
    }

    unsafe fn domctl<T: IsDomCtl>(&self, t: &mut T) -> Result<(), i32> {
        use core::ptr::addr_of_mut;
        let mut privcmd_arg = PrivCmdArg {
            op: super::DOMCTL_CMD as PrivCmdField,
            args: [addr_of_mut!(*t) as PrivCmdField, 0, 0, 0, 0],
            #[cfg(not(target_os = "linux"))]
            ret: 0,
        };

        match ioctl::hypercall(self.0.as_raw_fd(), addr_of_mut!(privcmd_arg)) {
            Ok(_) => Ok(()),
            Err(errno) => Err(errno as i32),
        }
    }
}
