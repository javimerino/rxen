//! CLI utility for Xen state manipulation
//!
//! It's a rewrite of the logic currently present in `xl` and everything it
//! links against. `xr` is meant to be able to fully replace `xl` through a
//! compatibility switch, such that it can be seemlessly integrated
//! into existing systems through an alias, such as `alias xl="xr xl"`.

use libxr::{hyp, XenCall};
use std::process::exit;

use clap::{Parser, Subcommand};

/// xr is a stateless CLI that streamlines communicating with the Xen hypervisor
/// from the shell.
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct XrCli {
    #[command(subcommand)]
    command: XrCommands,
}

#[derive(Subcommand)]
enum XrCommands {
    /// Print Xen's console ring to stdout
    Dmesg {
        /// Clear the console buffer after reading it
	#[arg(short, long)]
        clear: bool
    },
}

fn main() {
    let Some(xi) = hyp::XenInterface::try_new() else {
        exit(1);
    };

    use XrCommands as xr;
    match XrCli::parse().command {
        xr::Dmesg { clear: _c } => {
            if let Some(s) = libxr::std::try_dmesg(&xi) {
                print!("{s}");
            }
	},
    };
}
